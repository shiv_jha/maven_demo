import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AlertWithFrame
{
	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException
	{
		
		WebDriver driver=new FirefoxDriver();

	driver.get("file:///E:/Training/capgemini/Low_level/AlertBoxWithIFrameDemo.html");
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	driver.switchTo().frame(driver.findElement(By.id("frame")));
	//first
	driver.findElement(By.id("alert")).click();
	Alert alert=driver.switchTo().alert();
	Thread.sleep(5000);
	System.out.println("alert message is "+alert.getText());
	alert.accept();
	
	//second one
	driver.findElement(By.id("confirm")).click();
	Alert confirm=driver.switchTo().alert();
	Thread.sleep(5000);
	System.out.println("alert message is "+confirm.getText());
	confirm.accept();
	
	
	//third one
	
		driver.findElement(By.id("prompt")).click();
		Alert prompt=driver.switchTo().alert();
		System.out.println("alert message is "+prompt.getText());
		prompt.sendKeys("This is automated");
		Thread.sleep(5000);
		prompt.accept();
	
	
		
		driver.switchTo().defaultContent();
		
		
		
		
		
		
		
		
//		
//		Thread.sleep(5000);
//
//		driver.findElement(By.id("alert")).click();
//		
//		Thread.sleep(5000);
//		
//		Alert alert=driver.switchTo().alert();
//		System.out.println("The alert message is : " + alert.getText());
//		alert.accept();
//		
//		Thread.sleep(5000);
//		
//		driver.findElement(By.id("confirm")).click();
//		
//		Thread.sleep(5000);
//		
//		Alert confirm=driver.switchTo().alert();
//		
//		confirm.accept();
//		
//		Thread.sleep(5000);
//		
//		driver.findElement(By.id("confirm")).click();
//		
//		Thread.sleep(5000);
//		
//		confirm=driver.switchTo().alert();
//		
//		confirm.dismiss();
//		
//		Thread.sleep(5000);
//		
//		driver.findElement(By.id("prompt")).click();
//		
//		Thread.sleep(5000);
//		
//		Alert prompt=driver.switchTo().alert();
//		
//		String text=prompt.getText();
//		System.out.println(text);
//		
//		prompt.sendKeys("Smith");
//		
//		Thread.sleep(5000);
//		
//		prompt.accept();
//		
//		Thread.sleep(5000);
//		
//		driver.findElement(By.id("prompt")).click();
//		
//		Thread.sleep(5000);
//		
//		prompt=driver.switchTo().alert();
//		
//		prompt.dismiss();
//		
//		Thread.sleep(5000);
//		
		driver.quit();
	}

}
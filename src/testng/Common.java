package testng;

import java.io.File;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

public class Common {

	public static  WebDriver driver;

	
	  @Parameters({"Browser","url"}) 
	  @BeforeSuite(groups={"init"},alwaysRun = true)
	  public void setUp(String sBrowser,String sUrl) {
		String browser=sBrowser.toLowerCase();
		switch(browser)
		{
		case "firefox":
			driver=new FirefoxDriver();
			break;
		
		case "chrome":
			//
			System.setProperty("webdriver.chrome.driver","E:\\Selenium\\Generic Jars\\chromedriver.exe");
			driver=new ChromeDriver();
		
		case "ie":
			final DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			System.setProperty("webdriver.ie.driver","E:\\Training\\capgemini\\IEDriverServer.exe");
			driver=new InternetExplorerDriver(capabilities);
		
		case "phantomjs":
			//System.setProperty("phantomjs.binary.path", "E:\\Selenium\\Diebold\\phantomjs-2.1.1-windows\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");	
			//driver = new PhantomJSDriver();
			
		default:
			System.out.println("wrong browser choice, check testng.xml");
		
			///driver=new AndroidDriver();
		}
         
		  
		//driver=new FirefoxDriver();
		// System.setProperty("webdriver.chrome.driver","E:\\Selenium\\Generic Jars\\chromedriver.exe");
		//driver=new ChromeDriver();
	//final DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
	//capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	//System.setProperty("webdriver.ie.driver","E:\\Training\\capgemini\\IEDriverServer.exe");
	//driver=new InternetExplorerDriver(capabilities);
//	System.setProperty("phantomjs.binary.path", "E:\\Selenium\\Diebold\\phantomjs-2.1.1-windows\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");	
//		  
//			driver = new PhantomJSDriver();
		  
		  driver.get("https://demo.opencart.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("Title is " +driver.getTitle());
	  }	
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  // System.setProperty("phantomjs.binary.path", "E:\\Selenium\\Diebold\\phantomjs-2.1.1-windows\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");	
	  
	  //driver = new PhantomJSDriver();
		  
//			 System.setProperty("webdriver.chrome.driver","E:\\Selenium\\Generic Jars\\chromedriver.exe");
//			driver=new ChromeDriver();
//			driver.get("https://demo.opencart.com");
//			
//		  
//		  final DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
//	      capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
//			 System.setProperty("webdriver.ie.driver","E:\\Training\\capgemini\\IEDriverServer.exe");
//				driver=new InternetExplorerDriver(capabilities);
//				driver.get("https://demo.opencart.com");
//				
//			
//			 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	//  }
}

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MultipleWindows {

		static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
	driver=new FirefoxDriver();
	driver.get("file:///E:/Training/capgemini/Low_level/sample.html");
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	String parentPage=driver.getWindowHandle();
	System.out.println("Parent page handle "+parentPage);
	driver.findElement(By.linkText("Yahoo")).click();
	Thread.sleep(5000);
	String childpage="";
	 Set sHandles=driver.getWindowHandles();
	 for (Object s : sHandles) {

		 if(!s.toString().equals(parentPage))
		 {
			 childpage=s.toString();
			 break;
		 }
		 
	}
	 
	driver.switchTo().window(childpage) ;
	System.out.println("Title:-"+driver.getTitle());
	driver.close();
	driver.switchTo().window(parentPage);
	driver.findElement(By.id("username")).sendKeys("hello");
	
	}

	
	//hjhjgu;
}


import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Test1 {

	///This is for commiting purpose
	static WebDriver driver;
	//static int x;
	public static void main(String[] args) throws IOException {
		//System.out.println("Hello World");
		
		driver=new FirefoxDriver();
//		driver.get("http://www.google.com");
//		System.out.println("title is :"+driver.getTitle());
//		System.out.println("handle is :"+driver.getWindowHandle());
//		//System.out.println("source is :"+driver.getPageSource());
////lst-ib
//		driver.findElement(By.id("lst-ib")).sendKeys("Selenium");
//		driver.close();
		
		
		
		driver.get("http://www.newtours.demoaut.com");
		System.out.println("title is :"+driver.getTitle());
		System.out.println("handle is :"+driver.getWindowHandle());
		driver.findElement(By.name("userName")).sendKeys("mercury");
		driver.findElement(By.name("password")).sendKeys("mercury");
		driver.findElement(By.name("login")).click();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		//driver.close();
		try
		{
			if(driver.findElement(By.name("passCount")).isDisplayed())
			{
				System.out.println("login passed");
			}
		}
		catch(NoSuchElementException e)
		{
			System.out.println("login failed");
			
			File f1=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(f1, new File("E:\\Training\\capgemini\\home_page.png"));
		}
		
		new Select(driver.findElement(By.name("passCount"))).selectByVisibleText("4");
		driver.findElement(By.xpath("//*[@value='oneway']")).click();
		new Select(driver.findElement(By.name("fromPort"))).selectByVisibleText("London");
		new Select(driver.findElement(By.name("fromMonth"))).selectByVisibleText("March");
		new Select(driver.findElement(By.name("toPort"))).selectByVisibleText("Paris");
		new Select(driver.findElement(By.name("toMonth"))).selectByVisibleText("April");
		new Select(driver.findElement(By.name("airline"))).selectByVisibleText("Unified Airlines");
		new Select(driver.findElement(By.name("fromPort"))).selectByVisibleText("London");
		driver.findElement(By.xpath("//*[@value='Business']")).click();
		
		
		WebDriverWait wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.name("findFlights")));
		
		
		
		driver.findElement(By.name("findFlights")).click();
		

		//driver.findElement(By.xpath("//*[@value='oneway']")).sendKeys("dem");
	
		String x=driver.findElement(By.name("billAddress1")).getAttribute("value");
		System.out.println("Address from application is " + x);
	}
	
	

}
